/*
ES6
	ECMAScript
		- a standard  for scripting languanges lie Javascript
		- ES5
			- current  version implented into most browsers.

	1. let and const
	2. Destructuring
		- allows us  to break apart key structure into variable
		- Array Destructuring
*/
let employee = ['Sotto', 'Tito', 'Senate President', 'Male'];

let [lastName, firstName, position, gender] = employee; //one to one mapping

console.log(lastName);
console.log(position);

let player2  = ['Curry', 'Lillard', 'Paul', 'Irving'];

const [pointGuard1, pointGuard2, pointGuard3,  pointGuard4] = player2;
console.log(pointGuard1);
console.log(pointGuard2);
console.log(pointGuard3);
console.log(pointGuard4);

// Object destructuring - it allows us to destructure an object by allowing to add the values of  an object's property into respectictive variables.

// option1
let pet = {
	species: 'dog',
	color: 'brown',
	breed: 'German Sheperd',
}

// let {breed, color} = pet;
// alert(`I have  ${breed} dog and it's color ${color}`);
getPet({
	species: 'dog',
	color: 'black',
	breed: 'Doberman'
});

// option2
// function getPet(options){
// 	alert(`I have  ${options.breed} dog and it's color ${options.color}`);
// };

// option3
// function getPet(options){
// 	let breed = options.breed;
// 	let color = options.color;
// 	console.log(`I have ${breed}dog and it's color is ${color}`);
// }

// option4
// function getPet(options){
// 	let{breed, color} = options;
// 	console.log(`I have ${breed}dog and it's color is ${color}`);
// }

// option5
function getPet({breed, color}){
	console.log(`I have ${breed}dog and it's color is ${color}`);
}

// without function
let getPet2 = {
	species: 'dog',
	color: 'brown',
	breed: 'German Sheperd'
};

let {breed, color} = getPet2;
console.log(`I have ${breed}dog and it's color is ${color}`);

/* mini activity */

letperson({
	name: 'Gusion',
	birthday: 'June 20, 2000',
	age: 21,
});

function letperson(options){
	let sentence1 = `Hi I'm ${options.name}`;
	let sentence2 = `I was born on ${options.birthday}`;
	let sentence3 = `I am ${options.age}`;

	console.log(sentence1, sentence2, sentence3);
}

let pokemon1 = {
	name1: 'Charmander',
	level: 15,
	type: 'fire',
	moves: ['ember', 'scratch', 'leer']
};

const {name1, level, type, moves} =  pokemon1;
let sentence4  = `My pokemon is ${name1}. Its is level
${level}. It is a ${type} and it movements ${moves}.`
// what data type is moves?
console.log(moves);
const [move1,,move3] = moves;
console.log(move1);
console.log(move3);

pokemon1.name1 = 'Bulbasaur';
console.log(name1);
console.log(pokemon1);

// Arrow Functions - fat arrow
// before ES6
function printFullName1 (firstName1, middleInitial1, lastName1){
	return firstName1 + ' ' + middleInitial1 + ' ' + lastName1;
}

// In ES6
const printFullName = (firstName, middleInitial, lastName) => {
	return firstName + ' ' + middleInitial + ' ' + lastName;
}

// traditional functions
function displayMsg(){
	console.log('Hello world');
}

// ES6 functions

const hello = () => {
	console.log('Hellow from arrow.');
}

hello();

// traditional function
// function greet(pokemon1){
// 	console.log(`Hi, ${pokemon1.name1}!`)
// };
// greet(pokemon1)


// ES6 Arrow(=>) Function
const greet = (pokemon1) => {
	console.log(`Hi, ${pokemon1.name1}!`);
};
greet(pokemon1);

// Implicit Return - allows us to return a value without the use of return keyword.

// traditional function
// function addNum(num1, num2){
// 	console.log(num1 + num2);
// };

// convert to arrow function es6
const addNum = (num1, num2) => num1 + num2;
let sum = addNum(5, 6);
console.log(sum);

// this keyword
// arrow functions es6
let protagonist ={
	name: 'Cloud strife',
	occupation: 'Soldier',
	greet : function(){
		// tradiditional methods would have this keyword refere to the parent object
		console.log(this);
		console.log(`Hi! I'm ${this.name}.`);
	},
	// Arrow Function ES6 (show all window in webpage)
	indtroduceJob: () => {
		console.log(this);
		console.log(`I work as ${this.occupation}.`);
	} 
}
protagonist.greet();
protagonist.indtroduceJob();

// Class-base Object Blueprints
	// Classes are template of objects

	// Create a class
		//The constructor is a special method for creating and initializing an object.
	// Tradition function metho
	function Pokemon(name, type, level){
		this.name = name;
		this.type = type;
		this.level = level;
	};

	// ES6 Class creating
	class Car{
		constructor(brand, name, year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	};
	let car1 = new Car('Toyota', 'Vios', '2002');
	console.log(car1);

	let car2 = new Car('Cooper', 'Mini', '1969');
	console.log(car2);

	let car3 = new Car('Porsche', '911', '1960');
	console.log(car3);

===============================================================================
Activity:
1. Update and Debug the following codes to ES6
		Use template literals
		Use array/object destructuring
		Use arrow function
2. Create a class constructor able to receive 3 arguments
		- it should be able to receive two strings and a number
		- Using the this keywoed assign properties:
			name,
			breed,
			dogAge = <7 * human years>
				- assign the parameters as values to each property.
3. Create 2 new objects using our class constructor
	This constructor shoule be able to crate Dog objects.
	Log the 2 new Dog objects in the console or alert.

  let student1 = {
	name: "Shawn Michaels"
	birthday: "May 5, 2003',
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101" "Social Sciences 201"]
}

let student2 = {
	name "Steve Austin",
	birthday: "June 15, 2001",
	age: 20
	isEnrolled: true
	classes: ["Philosphy 401", "Natural Sciences 402",
}

function introduce(student){

	//Note: You can destructure objects inside functions.

	console.log("Hi! " + "I'm " + student.name + " ." + " I am " + student.ages + " years old.");
	console.log("I study the following courses + " classes);
}

function getCube(num){

	console.log(Math.pow(num,3));

}

let cube = getCube(3);

console.log(cube)

let numArr = [15,16,32,21,21,2]

numArr.forEach(function(num){

	console.log(num);
})

let numsSquared = numArr.map(function(num){

	return num ** 2;

})

console.log(numSquared);


