
// Activity:
// 1. Update and Debug the following codes to ES6
// 		Use template literals
// 		Use array/object destructuring
// 		Use arrow function
// 2. Create a class constructor able to receive 3 arguments
// 		- it should be able to receive two strings and a number
// 		- Using the this keyword assign properties:
// 			name,
// 			breed,
// 			dogAge = <7 * human years>
// 				- assign the parameters as values to each property.
// 3. Create 2 new objects using our class constructor
// 	This constructor shoule be able to crate Dog objects.
// 	Log the 2 new Dog objects in the console or alert.

let student1 = {
	name1: "Shawn Michaels",
	birthday1: "May 5, 2003",
	age1: 18,
	isEnrolled1: true,
	classes1: ["Philosphy 101", "Social Sciences 201"]
};

let student2 = {
	name2: "Steve Austin",
	birthday2: "June 15, 2001",
	age2: 20,
	isEnrolled2: true,
	classes2: ["Philosphy 401", "Natural Sciences 402"]
};

// >>>>>>>>>>>>>>>>>> literal <<<<<<<<<<<<<<<<<<<< \\
function introduce(student){

	//Note: You can destructure objects inside functions.

 console.log(`Hi I'm ${student1.name1}. I am ${student1.age1}years old.`);
 console.log(`I study the following courses ${student1.classes1}`)
 console.log(`Hi I'm ${student2.name2}. I am ${student2.age2}years old.`);
 console.log(`I study the following courses ${student2.classes2}`)	
};
const {name1, birthday1,  age1, isEnrolled1, classes1} =student1;
const {name, birthday,  age, isEnrolled, classes} =student2;
console.log(`Hi I'm ${student1.name1}. I am ${student1.age1}years old.`);
console.log(`I study the following courses ${student1.classes1}`)
console.log(`Hi I'm ${student2.name2}. I am ${student2.age2}years old.`);
console.log(`I study the following courses ${student2.classes2}`)

// >>>>>>>>>>>>>>>>>> literal <<<<<<<<<<<<<<<<<<<< \\

function getCube(num){
	console.log(Math.pow(num,3));
}
let cube = getCube(3);

const getCube1 = (num1) => (Math.pow(num1,3));
let cube1 = getCube1(3);
console.log(cube1);

// >>>>>>>>>>>>>>>>>> array/object destructuring <<<<<<<<<<<<<<<<<<<< \\

let numArr = [15,16,32,21,21,2];
	numArr.forEach(function(num){
	console.log(num);
})

const numArr1 = [15,16,32,21,21,2];
const array = numArr1.map((numArr1) => numArr1);
console.log(array);

// >>>>>>>>>>>>>>>>>> arrow <<<<<<<<<<<<<<<<<<<<

let numsSquared = numArr.map(function(num){
	return num ** 2;
	console.log(numSquared);
})

const numArr3 = [15,16,32,21,21,2];
const numSquared1 = numArr3.map((numArr3) => numArr3 ** 2)
console.log(numSquared1);

// >>>>>>>>>>>>>>>>>> class <<<<<<<<<<<<<<<<<<<< \\

class Dog{
	constructor(name, breed, dogAge){
		this.name = name;
		this.breed = breed;
		this.dogAge = dogAge;  // <7 * human years>	
	}
};
	
class Birth{
	constructor(month, day, year){
		this.month = month;
		this.day = day;
		this.year = year;
	}
};

	let dogAge1 = new Date("06/24/2014");  
    let dogAge2 = new Date("04/10/2018");
    let dogAge3 = new Date("02/18/2020");

    //calculate month difference from current date in time  
    let month_diff = Date.now() - dogAge1.getTime();  
    let month_diff1 = Date.now() - dogAge2.getTime();  
    let month_diff2 = Date.now() - dogAge3.getTime();  
      
    //convert the calculated difference in date format  
    let born_dt = new Date(month_diff);   
    let born_dt1 = new Date(month_diff1);   
    let born_dt2 = new Date(month_diff2);   
      
    //extract year from date      
    let year = born_dt.getUTCFullYear();  
    let year1 = born_dt1.getUTCFullYear();  
    let year2 = born_dt2.getUTCFullYear();  
      
    //now calculate the age of the dog  
    let born = Math.abs(year - 1970);
    let born1 = Math.abs(year1 - 1970);
    let born2 = Math.abs(year2 - 1970);
    
      
let Dog1 = new Dog ("Bite", "Pitbull", born  + " " + "years")
console.log(Dog1);

// >>>>>>>>>>>>>>>>>> add two dog <<<<<<<<<<<<<<<<<<<< \\

let Dog2 = new Dog ("Cooper", "Bulldog", born1  + " " + "years")
console.log(Dog2);

let Dog3 = new Dog ("Blacky", "K-9", born2  + " " + "years")
console.log(Dog3);